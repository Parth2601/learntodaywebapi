﻿using LearnTodayWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnTodayWebApi.Controllers
{
    public class AdminController : ApiController
    {
        public IEnumerable<Course> GetAllCourses()
        {
            using (LearnTodayWebAPIDbContext dbContext= new LearnTodayWebAPIDbContext())
            {
                return dbContext.Courses.ToList();
            }
        }
        public HttpResponseMessage GetCourseById(int id)
        {
            using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
            {
                var entity = dbContext.Courses.FirstOrDefault(s => s.CourseId == id);
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Searched data not found");
                }
            }
        }
    }
}
