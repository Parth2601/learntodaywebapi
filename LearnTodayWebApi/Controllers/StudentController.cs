﻿using LearnTodayWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnTodayWebApi.Controllers
{
    public class StudentController : ApiController
    {
        public IEnumerable<Course> GetAllCourses()
        {
            using(LearnTodayWebAPIDbContext dbContext=new LearnTodayWebAPIDbContext())
            {
               var env = dbContext.Courses.OrderByDescending(c => c.Start_Date).ToList();
                return env;
            }
        }
        public HttpResponseMessage Post([FromBody] Student student)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                    dbContext.Students.Add(student);
                    dbContext.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, student);
                    return message;
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                    var entity = dbContext.Students.FirstOrDefault(s => s.EnrollmentId == id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No enrollment information found");
                    }
                    else
                    {
                        dbContext.Students.Remove(entity);
                        dbContext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Record Deleted");

                    }
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
