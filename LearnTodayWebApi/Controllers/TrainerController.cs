﻿using LearnTodayWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnTodayWebApi.Controllers
{
    public class TrainerController : ApiController
    {
        public HttpResponseMessage Post([FromBody] Trainer trainer)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                    dbContext.Trainers.Add(trainer);
                    dbContext.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, trainer);
                    return message;
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdatePassword(int id, [FromBody] Trainer trainer)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                     Trainer entity = dbContext.Trainers.FirstOrDefault(s => s.TrainerId == id);
                    if(entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No record found to update");
                 
                    }
                    else
                    {
                        entity.Password = trainer.Password;
                        dbContext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
